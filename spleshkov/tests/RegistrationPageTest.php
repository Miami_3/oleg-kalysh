<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationPageTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRegistrationPageAvailable()
    {
        $response = $this->call('GET', '/registration/create');

        $this->assertEquals(200, $response->status());
    }
}
